# sdl2-entt-bullet

SDL2 project using EnTT and Bullet Physics with the latest OpenGL.

This project makes use of the following libraries/projects:

* [SDL2](https://www.libsdl.org/)
* [GLM](https://github.com/g-truc/glm)
* [CPM](https://github.com/cpm-cmake/CPM.cmake)
* [GLAD](https://glad.dav1d.de/)
* [EnTT](https://github.com/skypjack/entt/)
* [BULLET3](https://github.com/bulletphysics/bullet3)

![Screenshot](sdl2-entt-bullet.gif)
