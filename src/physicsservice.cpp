#include "physicsservice.hpp"

#include <entities.hpp>

struct EntityData
{
    entt::entity entity;
};

PhysicsService::PhysicsService()
{
    mCollisionConfiguration = new btDefaultCollisionConfiguration();
    mDispatcher = new btCollisionDispatcher(mCollisionConfiguration);

    mBroadphase = new btDbvtBroadphase();

    mSolver = new btSequentialImpulseConstraintSolver();

    mDynamicsWorld = new btDiscreteDynamicsWorld(mDispatcher, mBroadphase, mSolver, mCollisionConfiguration);
    mDynamicsWorld->setGravity(btVector3(0, 0, -9.81f));
    mDynamicsWorld->getBroadphase()->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());
}

PhysicsService::~PhysicsService()
{
    auto &objs = mDynamicsWorld->getCollisionObjectArray();

    for (int i = 0; i < objs.size(); i++)
    {
        auto entityData = reinterpret_cast<EntityData *>(objs[i]->getUserPointer());
        if (entityData == nullptr)
        {
            continue;
        }

        objs[i]->setUserPointer(nullptr);
        delete entityData;
    }
}

void PhysicsService::Step(
    entt::registry &reg,
    std::chrono::nanoseconds diff)
{
    static double time = 0;
    time += static_cast<double>(diff.count() / 1000000.0);
    while (time > 0)
    {
        mDynamicsWorld->stepSimulation(btScalar(1000.0f / 60.0f), 1);

        time -= (1000.0 / 60.0);
    }

    auto &objs = mDynamicsWorld->getCollisionObjectArray();

    for (int i = 0; i < objs.size(); i++)
    {
        auto entityData = reinterpret_cast<EntityData *>(objs[i]->getUserPointer());
        auto &phys = reg.get<PhysicsComponent>(entityData->entity);
        objs[i]->getWorldTransform().getOpenGLMatrix(&(phys.matrix[0][0]));
    }
}

void PhysicsService::AddCube(
    entt::entity entity,
    float mass,
    const glm::vec3 &size,
    const glm::vec3 &startPos)
{
    btVector3 fallInertia(0, 0, 0);

    auto shape = new btBoxShape(btVector3(size.x * 0.5f, size.y * 0.5f, size.z * 0.5f));
    shape->calculateLocalInertia(mass, fallInertia);

    btRigidBody::btRigidBodyConstructionInfo rbci(mass, nullptr, shape, fallInertia);
    rbci.m_startWorldTransform.setOrigin(btVector3(startPos.x, startPos.y, startPos.z));

    auto body = new btRigidBody(rbci);

    _entityBodyMap.insert(std::make_pair(entity, body));

    auto entityData = new EntityData();
    entityData->entity = entity;
    body->setUserPointer((void *)entityData);

    mDynamicsWorld->addRigidBody(body);
}

void PhysicsService::ApplyForce(
    entt::entity entity,
    const glm::vec3 &force,
    const glm::vec3 &relativePosition)
{
    if (_entityBodyMap.find(entity) == _entityBodyMap.end())
    {
        return;
    }

    _entityBodyMap[entity]->applyForce(
        btVector3(force.x, force.y, force.z),
        btVector3(relativePosition.x, relativePosition.y, relativePosition.z));
}
