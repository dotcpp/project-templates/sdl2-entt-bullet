#ifndef PHYSICSSERVICE_H
#define PHYSICSSERVICE_H

#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include <btBulletDynamicsCommon.h>
#include <chrono>
#include <glm/glm.hpp>
#include <iphysicsservice.hpp>

class PhysicsService : public IPhysicsService
{
public:
    PhysicsService();

    virtual ~PhysicsService();

    virtual void Step(
        entt::registry &reg,
        std::chrono::nanoseconds diff);

    virtual void AddCube(
        entt::entity entity,
        float mass,
        const glm::vec3 &size,
        const glm::vec3 &startPos);

    virtual void ApplyForce(
        entt::entity entity,
        const glm::vec3 &force,
        const glm::vec3 &relativePosition = glm::vec3());

private:
    btBroadphaseInterface *mBroadphase = nullptr;
    btDefaultCollisionConfiguration *mCollisionConfiguration = nullptr;
    btCollisionDispatcher *mDispatcher = nullptr;
    btSequentialImpulseConstraintSolver *mSolver = nullptr;
    btDiscreteDynamicsWorld *mDynamicsWorld = nullptr;
    std::map<entt::entity, btRigidBody*> _entityBodyMap;
};

#endif // PHYSICSSERVICE_H
