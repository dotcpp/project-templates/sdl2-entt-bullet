#ifndef IPHYSICSSERVICE_H
#define IPHYSICSSERVICE_H

#include <chrono>
#include <entt/entt.hpp>
#include <glm/glm.hpp>

class IPhysicsService
{
public:
    virtual ~IPhysicsService() = default;

    virtual void Step(
        entt::registry &reg,
        std::chrono::nanoseconds diff) = 0;

    virtual void AddCube(
        entt::entity entity,
        float mass,
        const glm::vec3 &size,
        const glm::vec3 &startPos) = 0;

    virtual void ApplyForce(
        entt::entity entity,
        const glm::vec3 &force,
        const glm::vec3 &relativePosition = glm::vec3()) = 0;
};

#endif // IPHYSICSSERVICE_H
