#ifndef ENTITIES_HPP
#define ENTITIES_HPP

#include <entt/entt.hpp>
#include <glm/glm.hpp>

struct MeshComponent
{
    size_t first;
    size_t count;
};

struct PhysicsComponent
{
    glm::mat4 matrix;
};


#endif // ENTITIES_HPP
