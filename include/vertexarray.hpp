#ifndef VERTEXARRAY_H
#define VERTEXARRAY_H

#include <glm/glm.hpp>
#include <tuple>
#include <vector>

class VertexArray
{
public:
    VertexArray();

    std::tuple<size_t, size_t> add(
        const float vertexData[],
        unsigned int vertexCount,
        const glm::vec3 &size = glm::vec3(1.0f),
        const glm::vec3 &transform = glm::vec3(0.0f));

    void upload();

    void bind();

    void cleanup();

    void render(
        size_t first,
        size_t count);

private:
    unsigned int _vao = 0, _vbo = 0;
    std::vector<float> _vertexData;
};

#endif // VERTEXARRAY_H
